#!/usr/bin/env python3
import re
import requests

class Web:
    def __init__(self):
        self.url = ''
        self.status = {}
    
    def setURL(self):
        with open("../URL","r") as f:
            data = f.readlines()
        for l in data:
            if re.match(r'.*\ (http.*).*',l):
                self.url = re.match(r'.*\ (http.*amazonaws.com).*',l).group(1)
    def setStatus(self):
        r = requests.get(self.url)
        if r.status_code == 200:
            self.status = { "status_code": r.status_code, 
                            "version": r.headers["Server"]
            }
        else:
            self.status = { "status_code": r.status_code, 
                            "error": r.text 
            }

if __name__ == "__main__":
    web = Web()
    web.setURL()
    web.setStatus()
    print(web.status)