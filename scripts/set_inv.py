#!/usr/bin/env python3
import re
with open("../ip_public","r") as f:
    data = f.readlines()
for l in data:
    if re.match(r'.*([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).*',l):
        ip = re.match(r'.*\ ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).*',l).group(1)

with open("../ansible/inventario.ini","w") as f2:
    f2.write("{} ansible_ssh_private_key_file=challenge-usintegrity.pem ansible_ssh_user=ubuntu".format(ip))