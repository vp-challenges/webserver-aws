terraform {
    backend "s3" {
        bucket = "challenge-usintegrity"
        dynamodb_table = "tf-lock-challenge-usintegrity"
        region = "us-east-1"
        key = "tf-webserver/terraform.tfstate"
        
    }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.10"
    }
  }
}