resource "aws_lb" "alb" {
  name                          = "alb-webserver"
  internal                      = false
  load_balancer_type            = "application"
  security_groups               = [aws_security_group.sg_alb.id]
  idle_timeout    = "300"
  subnets = [var.subnet_id, "subnet-08a5e3c432056397c"]
  enable_cross_zone_load_balancing = true

}


resource "aws_lb_target_group" "tg_alb_80" {
  name = "tg-alb-80"
  port = 8080
  protocol = "HTTP"
  vpc_id = var.vpc_id
  health_check {
    enabled = true
    matcher = "200-210,300-310"
  }
}


resource "aws_lb_listener" "ALB_Forward_80" {
  load_balancer_arn             = aws_lb.alb.arn
  
  default_action {
    type                        = "forward"
    target_group_arn            = aws_lb_target_group.tg_alb_80.arn
  }
  port                          = 80
  protocol                      = "HTTP"
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.tg_alb_80.arn
  target_id        = aws_instance.web.id
  port             = 8080
}