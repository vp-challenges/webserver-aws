data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


data "template_file" "instance_config" {
   template = file("./scripts/config.sh")
}

resource "aws_network_interface" "eth0_webserver" {
  subnet_id       = var.subnet_id
  security_groups = [aws_security_group.webserver.id] 
}


resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "challenge-usintegrity"
  user_data = data.template_file.instance_config.rendered

  network_interface {
    network_interface_id = aws_network_interface.eth0_webserver.id
    device_index         = 0
  }


  tags = {
    Name = "WebServer"
  }
} 
