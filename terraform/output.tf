output "ip_public_ec2" {
  value = aws_instance.web.public_ip
}

output "URL" {
  value = "http://${aws_lb.alb.dns_name}"
}