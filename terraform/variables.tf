variable "region" {
  description = "Target region"
  default = "us-east-1"
}

variable "profile" {
    description = "The profile you want to use"
}

variable "vpc_id" {
  description = "VPC-ID"
}

variable "subnet_id" {
  description = "subnet-id"
}