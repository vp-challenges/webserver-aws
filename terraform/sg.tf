resource "aws_security_group" "webserver" {
  name        = "webserver"
  description = "SG webserver"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Allow HTTP"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    security_groups = [ aws_security_group.sg_alb.id ]
  }


  ingress {
    description      = "Allow SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "SG_WebServer"
  }
}

resource "aws_security_group" "sg_alb" {
  name_prefix = "webserver_sg_alb_"
  vpc_id      = var.vpc_id
}

###Rule SG ALB
resource "aws_security_group_rule" "sgrule_alb_allow_http" {
  type = "ingress"
  security_group_id = aws_security_group.sg_alb.id
  from_port = 80
  to_port =  80
  protocol = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ]
  description = "Allow traffic to HTTP"
}

resource "aws_security_group_rule" "sgrule_alb_allow_outgoing" {
  type = "egress"
  security_group_id = aws_security_group.sg_alb.id
  from_port = 0
  to_port =  0
  protocol = "-1"
  cidr_blocks = [ "0.0.0.0/0" ]
  description = "Allow Outgoing traffic to Internet"
}
