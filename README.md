#Instructions to deploy WebServer

##Requirements
- AWS CLI Installed
- Python3 Installed
- Ansible Installed
- Terraform Installed

0. Set AWS Credentials
```
aws configure --profile=aws-evaluador
AWS Access Key ID [None]: <Complete with credential>
AWS Secret Access Key [None]: <Complete with credential>
Default region name [None]: us-east-1
Default output format [None]: <empty>
```

1. Download repo 

	`git clone git@gitlab.com:vp-challenges/webserver-aws.git `	 
 
2.  Create WebServer 
```
cd webserver-aws/terraform
terraform init
terraform plan --var-file=var.tfvars
terraform apply --var-file=var.tfvars --auto-approve 
terraform output | grep "ip_public_ec2 =" | tee ../ip_public
terraform output | grep "URL" | tee ../URL
cd ..
```

3. Deploy configuration  
```
cd ansible
chmod a+x ../scripts/set_inv.py
../scripts/set_inv.py
chmod 600 challenge-usintegrity.pem
ansible-playbook -i inventario.ini playbooks/install-dockercompose.yaml
ansible-playbook -i inventario.ini playbooks/deploy-nginx.yaml
ansible-playbook -i inventario.ini playbooks/nginx-service-start.yaml
cd ..

```
4. Check WebServer
```
cd scripts
chmod a+x check_service.py
./check_service.py

```